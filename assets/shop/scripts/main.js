import $ from "jquery";
import bootstrap from 'bootstrap/dist/js/bootstrap';
//import Tooltip from 'bootstrap/js/src/tooltip';
//import popover from 'bootstrap/js/src/popover';

$(document).ready(function () {
    $(window).scrollTop(0); // Inicia en posición 0 para animación de botella

    if ($(".homepage").length) {
        //Abrir modal al iniciar
        var popupModal = new bootstrap.Modal(document.getElementById('popupModal'));
        popupModal.show();
        //$('#popupModal').show();

        var title = ""
        var scrolled = 0;
        $(window).scroll(function () {

            var scroll = $(window).scrollTop();

            if (scroll > 0) {
                $('header').addClass('active');
            } else {
                $('header').removeClass('active');
            }

            // BOTELLA
            var fps = 30;
            var frames = $(".bottle-scroll img").length;
            var nextImageChange = scrolled + fps;
            var prevImageChange = scrolled - fps;

            // Giro
            if (scroll >= nextImageChange & scrolled < fps * (frames - 1)) {
                scrolled = nextImageChange;

                var nextFrame = $('.bottle-scroll img.active').next('img');
                $('.bottle-scroll img').removeClass('active');
                nextFrame.addClass('active')
            }

            // Giro inverso
            if (scroll <= prevImageChange & scrolled < fps * (frames + 1) & scroll >= 0) {
                scrolled = prevImageChange;

                var prevFrame = $('.bottle-scroll img.active').prev('img');
                $('.bottle-scroll img').removeClass('active');
                prevFrame.addClass('active')
            }

            // Cambio de Textos
            var bottleTitle = $('.bottle-title');
            var changeText = (fps * (frames - 1)) / 3;

            if (scroll <= changeText) {
                title = bottleTitle.data('title1');
                $('.bottle-bullets span').removeClass('active');
                $('.bottle-bullets span:nth-child(1)').addClass('active');
            } else if (scroll <= changeText * 2) {
                title = bottleTitle.data('title2');
                $('.bottle-bullets span').removeClass('active');
                $('.bottle-bullets span:nth-child(2)').addClass('active');
            } else if (scroll <= changeText * 3) {
                title = bottleTitle.data('title3');
                $('.bottle-bullets span').removeClass('active');
                $('.bottle-bullets span:nth-child(3)').addClass('active');
            }
            animateBottleTitle(bottleTitle, title);

            // Finaliza animación
            if (scroll >= 0 & scroll < fps * (frames - 1)) {
                var homeHeight = $('.homepage').height();
                $('body').height(homeHeight);
                $('.homepage').css({'position': 'fixed', 'marginTop': 0});
            } else if (scroll > fps * (frames - 1)) {
                var marginTop = $('.homepage').offset().top;
                $('.homepage').css({'position': 'relative', 'marginTop': marginTop});
            }


            /*
             console.log(scroll);
             if (scroll <= 0) {
             $('.bottle-scroll img').removeClass('active');
             $('.bottle-scroll img:first-of-type').addClass('active');
             } else if (scroll >= fps * (frames - 1)) {
             $('.bottle-scroll img').removeClass('active');
             $('.bottle-scroll img:last-of-type').addClass('active');
             }
             */
        });
    }

    $(document).on("submit", ".send", function (e) {
        e.preventDefault();
        let form = $(this);
        let container = $(this).data("container");
        console.log(container);
        $.ajax({
            url: form.attr('action'),
            data: new FormData(form[0]),
            processData: false,
            contentType: false,
            error: function () {
            },
            dataType: 'json',
            success: function (data) {

            },
            complete: function (data) {
                $(".send")[0].reset();
                $(".send .complete-"+container).removeClass("d-none");
            },
            type: 'POST'
        });
    });

    $('.scroll-link').click(function (event) {
        var href = $(this).attr('href');
        var scrollTime = 1000;

        $('#menuModal .close')[0].click();


        if ($(window).width() >= 992) {
            if (href == '#about-us') {
                $("html, body").animate({scrollTop: $(href).offset().top + 120}, scrollTime);
            } else if (href == '#about-sotol') {
                $("html, body").animate({scrollTop: $(href).offset().top - 70}, scrollTime);
            } else if (href == '#collection') {
                $("html, body").animate({scrollTop: $(href).offset().top + 200}, scrollTime);
            } else if (href == '#cocktails') {
                $("html, body").animate({scrollTop: $(href).offset().top - 85}, scrollTime);
            } else {
                $("html, body").animate({scrollTop: $(href).offset().top}, scrollTime);
            }
        } else {
            if (href == '#collection') {
                $("html, body").animate({scrollTop: $(href).offset().top + 220}, scrollTime);
            } else if (href == '#cocktails') {
                $("html, body").animate({scrollTop: $(href).offset().top - 60}, scrollTime);
            } else {
                $("html, body").animate({scrollTop: $(href).offset().top}, scrollTime);
            }
        }
    });

    if ($(".process-carousel").length) {
        if ($(window).width() < 992) {
            $('.process-carousel')[0].addEventListener('slid.bs.carousel', function () {
                var cPosition = $(this).find('button.active').text();
                $('.btn-collapse').text(cPosition);
            })
            $('.carousel-indicators button').click(function (event) {
                $('.btn-collapse')[0].click();
            });
        }
    }

    // Verificación
    if ($('.verification').length) {
        $('.languages a').click(function(event) {
            event.preventDefault();
            var lang = $(this).data('lang');
            if (lang == "es") {
                $(".v-title").text('¿Tienes edad suficiente para beber?');
                $("form button").text('Sí');
                $(".v-text").text('Debe ser mayor de edad para acceder a este sitio.');
                $(".v-go").attr('href', '/lang/es_MX');
            } else {
                $(".v-title").text('Are you old enough to drink?');
                $("form button").text('Yes');
                $(".v-text").text('You must be of legal drinking age in the country in which you are accessing this site.');
                $(".v-go").attr('href', '/lang/en_US');
            }
        });
    }
});

// Abrir modal en modal
$(document).on('click', '.js-modal-modal', function () {
    var modalClose = $(this).data('modal-close');
    var primaryModal = new bootstrap.Modal($(modalClose));
    primaryModal.hide();
    if ($('.modal-backdrop').is(':visible')) {
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
});

function animateBottleTitle(bottleTitle, title) {
    if (bottleTitle.text() != title) {
        bottleTitle.fadeOut('300', function () {
            $(this).text(title)
            $(this).fadeIn(300);
        });
    }
}
