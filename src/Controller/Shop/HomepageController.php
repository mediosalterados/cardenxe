<?php

declare(strict_types = 1);

namespace App\Controller\Shop;

use App\Entity\CommunityRegister;
use App\Form\RegisterType;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class HomepageController extends AbstractController {

    /** @var Environment */
    private $twig;

    public function __construct(Environment $twig) {
        $this->twig = $twig;
    }

    public function indexAction(Request $request): Response {

        //Formulario de registro
        $communityregister = new CommunityRegister();
        $form = $this->createForm(RegisterType::class, $communityregister);
        $session = $request->getSession();

        if ($session->get('verified')) {

            return new Response(
                    $this->twig->render('@SyliusShop/Homepage/index.html.twig', [
                        'frmregister' => $form->createView(),
                        'register' => $form->createView(),
                            ]
                    )
            );
        } else {
            return $this->redirectToRoute('verification');
        }
    }

   

}
