<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Form\VerificationType;
use App\Entity\CommunityRegister;
use App\Entity\Community;
use App\Entity\Newsletter;
use App\Form\RegisterType;
use App\Form\NewsletterType;
use App\Form\CommunityType;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController {

    /**
     * @Route("/", name="home")
     */
    public function index(Request $request) {
//        dd('lega');
        //Formulario de registro
        $communityregister = new CommunityRegister();
        $form = $this->createForm(RegisterType::class, $communityregister);
        $session = $request->getSession();

        if ($session->get('verified')) {


            return $this->render('@SyliusShop/home/index.html.twig', [
                        'frmregister' => $form->createView(),
                        'register' => $form->createView(),
                            ]
            );
        } else {
            return $this->redirectToRoute('verification');
        }
    }

    /**
     * @Route("/verification", name="verification")
     */
    public function indexVerification(Request $request) {
        $form = $this->createForm(VerificationType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $session = new Session();
            $session->set('verified', true);

            return $this->redirect('/' . $request->getLocale());
        }

        return $this->render('@SyliusShop/Verification/index.html.twig', [
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/interest", name="interest")
     */
    public function interest(Request $request) {
        $register = new CommunityRegister();
        $form = $this->createForm(RegisterType::class, $register);
        $form->handleRequest($request);
        $request->isXmlHttpRequest();
        $data = array();
        $data = $request->request->get('register');
        $inter = implode(", ", $data['interestC']);
        $inter = $inter . $data['otro'];

        if ($form->isSubmitted() && $form->isValid()) {
            $register->setInterest($inter);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($register);
            $entityManager->flush();
            $response = array('status' => "ok");
            $jsonResponse = new JsonResponse($response);
            return $jsonResponse;
        }
    }

    /**
     * @Route("/community-from", name="community-from")
     */
    public function communityFrom(Request $request) {
        $register = new Community();
        $form = $this->createForm(CommunityType::class, $register);
        $form->handleRequest($request);
        $request->isXmlHttpRequest();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($register);
            $entityManager->flush();
            $response = array('status' => "ok");
            $jsonResponse = new JsonResponse($response);
            return $jsonResponse;
        }
        return $this->render('@SyliusShop/Verification/community.html.twig', [
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/community-from", name="community-from")
     */
    public function community(Request $request) {
        $communityregister = new CommunityRegister();
        $form = $this->createForm(RegisterType::class, $communityregister);
        return $this->render('@SyliusShop/Verification/landing.html.twig', [
                    'register' => $form->createView(),
        ]);
    }

    /**
     * @Route("/newsletter", name="newsletter")
     */
    public function newsletter(Request $request) {
        $register = new Newsletter();
        $form = $this->createForm(NewsletterType::class, $register);
        $form->handleRequest($request);
        $request->isXmlHttpRequest();
//        dump($form->isValid());
//        dd($form->isSubmitted());
        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($register);
            $entityManager->flush();
            $response = array('status' => "ok");
            $jsonResponse = new JsonResponse($response);
            return $jsonResponse;
        }
        return $this->render('@SyliusShop/Verification/newsletter.html.twig', [
                    'newsletter' => $form->createView(),
        ]);
    }

    /**
     * @Route("/lang/{lang}", name="lang")
     */
    public function indexLang(Request $request, $lang = null): Response {
        if ($lang != null) {
            //$request->getSession()->set('_locale', $lang);
            //$request->setLocale($lang);
            $this->get('session')->set('_locale', $lang);
            $request->setLocale($lang);

            $session = new Session();
            $session->set('verified', true);
        }
        return $this->redirect('/' . $lang);
    }

}
