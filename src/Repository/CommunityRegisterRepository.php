<?php

namespace App\Repository;

use App\Entity\CommunityRegister;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CommunityRegister|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommunityRegister|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommunityRegister[]    findAll()
 * @method CommunityRegister[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommunityRegisterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommunityRegister::class);
    }

    // /**
    //  * @return CommunityRegister[] Returns an array of CommunityRegister objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommunityRegister
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
