<?php

namespace App\Form;

use App\Entity\CommunityRegister;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options): void {
        $builder
                ->add('name', null, [
                    'attr' => array(
                        'placeholder' => "modals.landing.name",
                    )
                ])
                ->add('email', EmailType::class, [
                    'attr' => array(
                        'placeholder' => "modals.landing.email"
                    )
                ])
                ->add('phone', null, [
                    'attr' => array(
                        'placeholder' => "modals.landing.Phone"
                    ),
                    'required' => false,
                ])
                ->add('city', null, [
                    'required' => true,
                    'attr' => array(
                        'placeholder' => "modals.landing.City"
                    )
                ])
                ->add('instagram', null, [
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'modals.landing.handle'
                    )
                ])
                ->add('propose', TextareaType::class, [
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'modals.landing.propose'
                    )
                ])
                ->add('otro', null, [
                    'mapped' => false,

                ])
                ->add('imageFile', VichFileType::class, [
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'Send us your shot'
                    )
                ])
                ->add('interestC', ChoiceType::class, array(
                    'multiple' => true,
                    'expanded' => true,
                    'required' => true,
                    'mapped' => false,
                    'choices' => array(
                        'modals.landing.painting' => 'painting',
                        'modals.landing.photography' => 'photography',
                        'modals.landing.culinary' => 'culinary',
                        'modals.landing.mixology' => 'mixology',
                        'modals.landing.music-production' => 'music-production',
                        'modals.landing.musical-instruments' => 'musical-instruments',
                        'modals.landing.acting' => 'acting',
                        'modals.landing.extreme' => 'extreme',
                        'modals.landing.poetry' => 'poetry',
                        'modals.landing.event' => 'landing.event',
                        'modals.landing.restaurateur' => 'restaurateur',
                        'modals.landing.foodie' => 'foodie',
                        'modals.landing.comedy' => 'comedy',
                        'modals.landing.sculpture' => 'sculpture',
                        'modals.landing.exploring' => 'exploring',
                        'modals.landing.parties' => 'parties',
                        'modals.landing.otro' => 'otro',
                    ),
                    'attr' => array(
                         'required' => true,
                        'name' => 'holii',
                    )
                ))
//            ->add('Join', SubmitType::class,[
//
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => CommunityRegister::class,
        ]);
    }

}
