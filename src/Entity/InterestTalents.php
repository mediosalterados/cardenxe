<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\InterestTalentsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=InterestTalentsRepository::class)
 */
class InterestTalents
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $comunityregister_id;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $intereststalents;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $other;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComunityregisterId(): ?int
    {
        return $this->comunityregister_id;
    }

    public function setComunityregisterId(int $comunityregister_id): self
    {
        $this->comunityregister_id = $comunityregister_id;

        return $this;
    }

    public function getIntereststalents(): ?string
    {
        return $this->intereststalents;
    }

    public function setIntereststalents(?string $intereststalents): self
    {
        $this->intereststalents = $intereststalents;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(?string $other): self
    {
        $this->other = $other;

        return $this;
    }
}
